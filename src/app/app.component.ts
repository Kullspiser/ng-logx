import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {NgLogx, LogLevel, LogType} from "ng-logx";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ng-logs';

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.syncOperation();
    this.syncOperation2();
    this.asyncOperation().subscribe();
  }

  @NgLogx({ type: LogType.SYNC, level: LogLevel.INFO })
  private syncOperation2(): string {
    return this.title + '123123';
  }

  @NgLogx({ type: LogType.SYNC, level: LogLevel.ERROR })
  private syncOperation(): string {
    return this.title;
  }

  @NgLogx({ type: LogType.ASYNC, level: LogLevel.WARNING })
  private asyncOperation(): Observable<any> {
    return this.http.get('/api/posts');
  }
}
