import {LogLevel, NgLogxInfoParameters, NgLogxParameters} from "./types";

export const isHasOptions = (params: NgLogxParameters): params is NgLogxInfoParameters => {
  return params.level === LogLevel.INFO && !!(params as NgLogxInfoParameters).options;
}
