export enum LogType {
  SYNC,
  ASYNC,
}

export enum LogLevel {
  INFO,
  WARNING,
  ERROR,
}

export interface NgLogxOptions {
  color?: string;
  fontFamily?: string;
  fontStyle?: string;
}

export interface NgLogxParameter {
  type: LogType,
  enable?: boolean;
  level?: LogLevel,
}

export interface NgLogxInfoParameters extends NgLogxParameter {
  level: LogLevel.INFO,
  options: NgLogxOptions;
}

export type NgLogxParameters =
  NgLogxParameter
  | NgLogxInfoParameters;
