import {NgLogxOptions} from "./types";

const DICTIONARY_TO_STYLES = {
  color: 'color',
  fontFamily: 'font-family',
  fontStyle: 'font-style'
};

export const prepareOptionsToLog = (options: NgLogxOptions): string => {
  return Object.keys(options).reduce((acc: string[], key: string) => {
    return [...acc, `${DICTIONARY_TO_STYLES[key]}:${options[key]}`];
  }, []).join(';');
};
