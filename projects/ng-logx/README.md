# NgLogx

This library provides a simple way for logging anything what you need.

## How to use

Just run this command in root of your project:

`npm install ng-logx`

Then you can import `NgLogx` decorator with your class method.

For instance:
    
    import { NgLogx } from "ng-logx";
    @NgLogx({ type: LogType.SYNC, level: LogLevel.INFO })
    private syncOperation(): string {
        return "Hello, World!";
    }

Or:

    import { NgLogx } from "ng-logx";
    @NgLogx({ type: LogType.ASYNC, level: LogLevel.WARNING })
    private asyncOperation(): Observable<any> {
        return this.http.get('/api/posts');
    }

## Options

Basically `@ngLogx` decorator takes parameter as `NgLogxParameters`.
Here we have possible options for that:

### `Level`
**Possible values**: `LogLevel.INFO`, `LogLevel.WARNING`, `LogLevel.ERROR` <br />
**Description**: `level` parameter related to visual style and type of your logged message.
It has relation with standard `Console` object.

### `type`
**Possible values**: `LogType.SYNC`, `LogType.ASYNC` <br />
**Description**: `type` related to type of your method - standard synchronized or implemented by RxJS's `Observable` type.

### `enable`
**Possible values**: `true` or `false` <br />
**Description**: `enable` option needs to turn off log feature if you don't need it at the moment.

### `options` - can use only with `level: LogLevel.INFO`
**Description**: `options` contains some CSS-like rules about how to display whole message.

